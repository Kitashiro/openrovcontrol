var io = require('socket.io-client');
var socket = io.connect('http://192.168.254.1:8080');
var XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;
var request = new XMLHttpRequest();

var time = function(){
		var controls = {};
		var priorControls = {};
		controls.throttle = 0;
		controls.yaw = 0;
		//controls.yaw = Math.min(Math.max(controls.yaw, -1), 1);
		controls.lift = 0;
		controls.pitch = 0;
		controls.roll = 0;
		
		for(var control in controls){
          	if(controls[control] != priorControls[control]){
            	socket.emit(control, controls[control]);
            	}
       		}
       		priorControls = controls;
    }


socket.on('connect', function(){

	var Loop = setInterval(function(){
	
		request.open('GET', 'http://192.168.100.53:8000', false);
		request.send(); // because of "false" above, will block until the request is done 
                // and status is available. Not recommended, however it works for simple cases.

		if (request.status === 200) {
  			console.log(request.responseText);
		}

		var a = request.responseText.split(',');
		console.log(a[0]);
	
		switch (a[0]){
		case 'data':
			var Forward = setTimeout(function(){
				var controls = {};
				var priorControls = {};
				controls.throttle = 1*0.25;
				controls.yaw = 0;
				controls.yaw = Math.min(Math.max(controls.yaw, -1), 1);
				controls.lift = 0;
				controls.pitch = 0;
				controls.roll = 0;
		
				for(var control in controls){
          			if(controls[control] != priorControls[control]){
            			socket.emit(control, controls[control]);
          			}
          		}
    			priorControls = controls;
   		
 				console.log('forward');
 				
 			},0);
 			
			setTimeout(time, a[1]);
			console.log('end');
 			break;
 			//if文でa[2]に文字列が含まれていた場合、emitで終了処理を行なったのち、格納された文字列を識別して他のcaseの中身と同じ処理を行う
 			
 		case 'data2':
 			var controls = {};
			var priorControls = {};
			controls.throttle = -1*0.25;
			controls.yaw = 0;
			controls.yaw = Math.min(Math.max(controls.yaw, -1), 1);
			controls.lift = 0;
			controls.pitch = 0;
			controls.roll = 0;
		
			for(var control in controls){
         		if(controls[control] != priorControls[control]){
            		socket.emit(control, controls[control]);
        			console.log('conectted');
      			}
			}
		    priorControls = controls;
   	
 			console.log('backward');
 			
			setTimeout(time, a[1]);
			console.log('end'); 
 			break;
		}
	},a[1]+100);
});

# This file is a template, and might need editing before it works on your project.
# Official framework image. Look for the different tagged releases at:
# https://hub.docker.com/r/library/node/tags/
image: node:latest

# Pick zero or more services to be used on all builds.
# Only needed when using a docker container to run your tests in.
# Check out: http://docs.gitlab.com/ce/ci/docker/using_docker_images.html#what-is-a-service
services:
  - mysql:latest
  - redis:latest
  - postgres:latest

# This folder is cached between builds
# http://docs.gitlab.com/ce/ci/yaml/README.html#cache
cache:
  paths:
  - node_modules/

test_async:
  script:
   - npm install
   - node ./specs/start.js ./specs/async.spec.js

test_db:
  script:
   - npm install
   - node ./specs/start.js ./specs/db-postgres.spec.js
